import {
  createConnection,
  TextDocuments,
  ProposedFeatures,
  Hover,
  TextDocumentPositionParams,
  MarkupKind,
  MarkupContent,
  CompletionItem,
  TextDocumentSyncKind,
  Connection,
  InsertTextFormat,
  SignatureHelp,
  SignatureHelpParams,
  SignatureInformation,
  CodeLensParams,
  CodeLens,
  DocumentFormattingParams,
  DocumentRangeFormattingParams,
  DocumentOnTypeFormattingParams,
  InitializeParams,
  InitializeResult,
  DidChangeConfigurationNotification,
  DidChangeConfigurationParams,
  FormattingOptions,
  Range,
  Position,
  Diagnostic,
  TextEdit,
  TextDocumentChangeEvent,
  DidChangeTextDocumentParams
} from 'vscode-languageserver/node'

import {
  TextDocument
} from 'vscode-languageserver-textdocument'
import { LangSpec, LangSpecItem } from './langSpec'
import { Rules } from './rules'

/*
 *
 * This section is for the connection
 * and its handlers.
 *
 */

const connection: Connection = createConnection(ProposedFeatures.all)
const documents: TextDocuments<TextDocument> = new TextDocuments(TextDocument)
const LangSpecCompletions: CompletionItem[] = buildCompletionList()
const LangSpecSignatures: SignatureInformation[] = buildSignatureList()
let hasConfigurationCapability: boolean = false
let hasWorkspaceFolderCapability: boolean = false
let currentSettings = {
  codeLens: null,
  statusBarTracking: null,
  formatting: {
    enable: {
      global: null,
      asTyping: null,
      onSave: null
    }
  },
  regionHighlight: null
}

connection.onInitialize((params: InitializeParams) => {
  const capabilities = params.capabilities

  hasConfigurationCapability = !!(capabilities?.workspace?.configuration ?? false)
  hasWorkspaceFolderCapability = !!(capabilities?.workspace?.workspaceFolders ?? false)

  const result: InitializeResult = {
    capabilities: {
      textDocumentSync: TextDocumentSyncKind.Incremental,
      hoverProvider: true,
      completionProvider: {
        resolveProvider: true
      },
      signatureHelpProvider: {
        triggerCharacters: ['(']
      },
      codeLensProvider: {
        resolveProvider: true
      },
      documentFormattingProvider: true,
      documentRangeFormattingProvider: true,
      documentOnTypeFormattingProvider: {
        firstTriggerCharacter: '}'
      }
    }
  }
  if (hasWorkspaceFolderCapability) {
    result.capabilities.workspace = {
      workspaceFolders: {
        supported: true
      }
    }
  }
  return result
})

connection.onInitialized((): void => {
  if (hasConfigurationCapability) {
    connection.client.register(DidChangeConfigurationNotification.type, undefined).then(() => {}, () => {})
    connection.workspace.getConfiguration({ section: 'jenkinsfile' }).then(val => { currentSettings = val }, () => {})
  }
  if (hasWorkspaceFolderCapability) {
    connection.workspace.onDidChangeWorkspaceFolders(_event => {
      console.log('Workspace folder change event received.')
    })
  }
})

connection.onDidChangeConfiguration((change: DidChangeConfigurationParams) => {
  connection.workspace.getConfiguration({ section: 'jenkinsfile' })
    .then(val => {
      currentSettings = val
      documents.all().forEach(runDocumentValidation)
    }, () => {})
})

connection.onHover((_pos: TextDocumentPositionParams): Hover | null => {
  const active = documents.get(_pos.textDocument.uri)
  if (active === undefined) {
    return null
  }
  const keyword = getWordAtCursor(active.getText(), active.offsetAt(_pos.position)).trimEnd()
  if (keyword !== '' && LangSpec[keyword] !== undefined) {
    const item = LangSpec[keyword]
    return getLangPopupMarkdown(keyword, item)
  }
  return null
})

connection.onCompletion((_pos: TextDocumentPositionParams): CompletionItem[] => {
  return LangSpecCompletions
})

connection.onCompletionResolve((_item: CompletionItem): CompletionItem => {
  return _item
})

connection.onSignatureHelp((params: SignatureHelpParams): SignatureHelp | null => {
  const active = documents.get(params.textDocument.uri)
  if (active === undefined) {
    return null
  }
  const symbol = getSymbolAtCursor(active.getText(), active.offsetAt(params.position))
  if (symbol !== '') {
    const sig = LangSpecSignatures.find(s => s.label.startsWith(symbol))
    if (sig !== undefined) {
      return {
        signatures: [sig]
      }
    }
  }
  return null
})

connection.onCodeLens((params: CodeLensParams): CodeLens[] | null => {
  return runCodeLens(params)
})

connection.onCodeLensResolve((params: CodeLens): CodeLens => {
  params.command = {
    title: params.data.path,
    command: ''
  }
  return params
})

documents.onDidOpen((params: TextDocumentChangeEvent<TextDocument>) => {
  runDocumentValidation(params.document)
})

documents.onDidChangeContent((params: TextDocumentChangeEvent<TextDocument>) => {
  runDocumentValidation(params.document)
})

documents.onDidClose((params: TextDocumentChangeEvent<TextDocument>) => {

})

connection.onDocumentFormatting((params: DocumentFormattingParams): TextEdit[] | null => {
  return runDocumentFormatting(params.textDocument.uri, params.options, null, null)
})

connection.onDocumentRangeFormatting((params: DocumentRangeFormattingParams): TextEdit[] | null => {
  return runDocumentFormatting(params.textDocument.uri, params.options, params.range, null)
})

connection.onDocumentOnTypeFormatting((params: DocumentOnTypeFormattingParams): TextEdit[] | null => {
  return runDocumentFormatting(params.textDocument.uri, params.options, null, params.position)
})

documents.listen(connection)
connection.listen()

/*
 *
 * Private methods
 *
 */

function getLangPopupMarkdown (key: string, item: LangSpecItem, hover: boolean = true): { contents: MarkupContent } {
  let content: string[]
  if (hover) {
    content = [
      `### ${key}`,
      '***',
      `${item.description}`,
      '```jenkinsfile'
    ].concat(item.usage).concat(['```'])
  } else {
    content = [
      '```jenkinsfile'
    ].concat(item.usage).concat(['```'])
  }
  if (item.details !== undefined) {
    content.push(`${item.details}`)
  }
  if (item.link !== undefined) {
    content.push('***')
    content.push(`[${item.link[0]}](${item.link[1]})`)
  }
  const markdown: MarkupContent = {
    kind: MarkupKind.Markdown,
    value: content.join('\n')
  }
  return {
    contents: markdown
  }
}

function getWordAtCursor (str: string, pos: number): string {
  const left = str.slice(0, pos + 1).search(/\w+$/)
  const right = str.slice(pos).search(/\W/)
  if (right < 0) {
    return str.slice(left)
  }
  return str.slice(left, right + pos)
}

function getSymbolAtCursor (str: string, pos: number): string {
  const left = str.slice(0, pos + 1).search(/\S+\(.+$/)
  const right = str.slice(left).search(/\(/)
  return str.slice(left, left + right)
}

function buildCompletionList (): CompletionItem[] {
  return Object.entries(LangSpec).map(i => {
    return {
      label: i[1].signature,
      kind: i[1].kind,
      labelDetails: {
        description: i[1].description
      },
      insertTextFormat: InsertTextFormat.Snippet,
      insertText: i[1].insertion,
      detail: i[1].description,
      documentation: getLangPopupMarkdown(i[0], i[1], false).contents
    }
  })
}

function buildSignatureList (): SignatureInformation[] {
  return Object.entries(LangSpec).filter(i => i[1].parameters !== undefined).map(i => {
    return {
      label: i[1].signature,
      documentation: i[1].description,
      parameters: i[1].parameters
    }
  })
}

function runCodeLens (params: CodeLensParams): CodeLens[] | null {
  if (currentSettings.codeLens !== true) {
    return null
  }
  const active = documents.get(params.textDocument.uri)
  if (active === undefined) {
    return null
  }
  const text = active.getText()
  const matches = [...text.matchAll(/([ \t]+)(stages|stage|post|steps|script|parallel|matrix)(( ?\(['"](.+)['"]\))+)?( ?{)/gm)]
  const lenses: CodeLens[] = []
  if (matches !== null) {
    const tempList: Array<{ indent: number, name: string, common: string, index: number }> = []
    for (const match of matches) {
      if (match.index !== undefined) {
        tempList.push({
          indent: match[1].length,
          name: match[2],
          common: match[5] ?? '',
          index: match.index
        })
      }
    }
    for (const item of tempList) {
      const pos = active.positionAt(item.index)
      let path = tempList.filter(i => i.indent < item.indent && i.index < item.index)
      path = path.filter(i => i.index === path.findLast(x => x.indent === i.indent && x.index <= item.index)?.index).toReversed()
      if (path.length > 0) {
        const toRemove: Array<{ indent: number, name: string, common: string, index: number }> = []
        let id = path[0].indent
        path.forEach(item => {
          if (item.indent > id) {
            toRemove.push(item)
          } else {
            id = item.indent
          }
        })
        path = path.filter(i => !toRemove.includes(i))
      }
      const intraPath = path.reverse().map(i => i.common === '' ? i.name : i.common)
      const endPath = item.common === '' ? item.name : item.common
      const segments = ['pipeline'].concat(intraPath).concat(endPath)
      lenses.push({
        range: {
          start: pos,
          end: pos
        },
        data: {
          path: segments.join(' -> ')
        }
      })
    }
  }
  return lenses
}

function runDocumentValidation (doc: TextDocument): void {
  let problems: Diagnostic[] = []
  Rules.forEach(item => {
    const result = item(doc)
    if (result !== null) {
      problems = problems.concat(result)
    }
  })
  connection.sendDiagnostics({ uri: doc.uri, diagnostics: problems }).then(() => {}, () => {})
}

function runDocumentFormatting (docUri: string, options: FormattingOptions, range: Range | null, position: Position | null): TextEdit[] | null {
  if (currentSettings.formatting.enable.global !== true) {
    return null
  }
  const doc = documents.get(docUri)
  if (doc === undefined) {
    return null
  }
  if (range === null && position === null) {
    // Whole doc
    return runFormatDoc(doc, options)
  }
  if (range === null && position !== null) {
    // As Type
    if (currentSettings.formatting.enable.asTyping !== true) {
      return null
    }
    return runFormatLocation(doc, options, position)
  }
  if (position === null && range !== null) {
    // Range
    return runFormatRange(doc, options, range)
  }
  return null
}

function runFormatDoc (doc: TextDocument, options: FormattingOptions): TextEdit[] | null {
  return null
}

function runFormatRange (doc: TextDocument, options: FormattingOptions, range: Range): TextEdit[] | null {
  return null
}

function runFormatLocation (doc: TextDocument, options: FormattingOptions, position: Position): TextEdit[] | null {
  return null
}
