export function makePipelineTree (doc: string): object {
  console.log(doc.length)
  const pipelineIdx = [...doc.matchAll(/pipeline ?{/gm)][0].index ?? 0
  console.log(pipelineIdx)
  const docLines = doc.split('\n')
  const regex = /^([ \t]*)([a-z]+)(?<!try|if|else|pipeline)(( ?\(['"](.+)['"]\))+)?( ?{)([^}\n]+)?(})?|^([ \t]+)(})/gm
  const tree = {
    pipeline: {}
  }
  let currIdx = 0
  for (const line of docLines) {
    const match = [...line.matchAll(regex)]
    if (match.length > 0 && currIdx >= pipelineIdx) {
      const m = match[0]
      if (m[9] !== undefined) {
        // Is end of a region
        const spaces = m[9].length
        const endIdx = currIdx + line.length + 1
        console.log('is end', spaces, endIdx)
      } else {
        // Is start of a region
        const spaces = m[1].length
        const tag = m[2]
        const tagInfo = m[5] ?? ''
        const startIdx = currIdx + spaces
        const endIdx = m[8] !== undefined ? currIdx + line.length + 1 : -1
        console.log('is begin', spaces, endIdx, tag, tagInfo, startIdx)
      }
    }
    currIdx += line.length + 1
  }
  console.log(currIdx - 1)
  return {}
}

export function detectNewlineType (doc: string): string {
  const cr = doc.match(/\r/gm)?.length ?? 0
  const lf = doc.match(/\n/gm)?.length ?? 0
  const crlf = doc.match(/\r\n/gm)?.length ?? 0
  console.log(cr, lf, crlf)
  if (crlf === cr && crlf === lf) {
    console.log('detected CRLF')
    return '\r\n'
  }
  if (cr > lf) {
    console.log('detected CR')
    return '\r'
  } else {
    console.log('detected LF')
    return '\n'
  }
}
