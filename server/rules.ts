import { TextDocument } from 'vscode-languageserver-textdocument'
import { Diagnostic, DiagnosticSeverity } from 'vscode-languageserver/node'

export const Rules = [
  oneOf
]

function oneOf (doc: TextDocument): Diagnostic[] | null {
  const text = doc.getText()
  const matches = [...text.matchAll(/(pipeline) ?{/gm)]
  const problems: Diagnostic[] = []
  if (matches.length > 0) {
    const tokens: { [k: string]: number[] } = matches.reduce((prev: { [k: string]: number[] }, curr: RegExpMatchArray): { [k: string]: number[] } => {
      if (curr.index !== undefined) {
        if (Object.keys(prev).includes(curr[1])) {
          prev[curr[1]].push(curr.index)
          return prev
        } else {
          prev[curr[1]] = [curr.index]
          return prev
        }
      }
      return prev
    }, {})
    Object.entries(tokens).forEach(value => {
      if (value[1].length > 1) {
        value[1].forEach(index => {
          const diagnostic: Diagnostic = {
            severity: DiagnosticSeverity.Error,
            range: {
              start: doc.positionAt(index),
              end: doc.positionAt(index + value[0].length)
            },
            message: `'${value[0]}' can only appear once.`,
            source: 'Jenkinsfile'
          }
          problems.push(diagnostic)
        })
      }
    })
    if (problems.length > 0) {
      return problems
    }
  }
  return null
}
