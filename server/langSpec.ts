import { CompletionItemKind, ParameterInformation } from 'vscode-languageserver/node'

export interface LangSpecItem {
  /**
   * Short description
   *
   * Used in completion and popups
   */
  description: string
  /**
   * Additional strings that are joined for codeblock
   *
   * For highlighting needs to be syntatically correct
   */
  usage: string[]
  /**
   * CompletionItemKind for code completion
   */
  kind: CompletionItemKind
  /**
   * the signature for code completion
   */
  signature: string
  /**
   * Insertion text
   */
  insertion: string
  /**
   * ParameterInformation array for items in ( )
   *
   * In order
   */
  parameters?: ParameterInformation[]
  /**
   * Additional details shown at bottom of a popup
   */
  details?: string
  /**
   * A two item array for link to docs.
   *
   * Item one is the name
   * Item two is the link
   *
   * Forms a markdown link tag
   */
  link?: [
    string, string
  ]
}

export const LangSpec: { [keyword: string]: LangSpecItem } = {
  pipeline: {
    description: 'The entrypoint for the job',
    kind: CompletionItemKind.Method,
    signature: 'pipeline',
    insertion: 'pipeline{\n\n}',
    usage: [
      'pipeline{',
      '  //More config',
      '}'
    ],
    link: ['Pipeline Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#declarative-pipeline']
  },
  stage: {
    description: 'A single phase of a job',
    details: 'There must be at least one in a stages directive',
    kind: CompletionItemKind.Method,
    signature: "stage('name')",
    insertion: "stage('name'){\n\n}",
    parameters: [
      {
        label: [6, 12],
        documentation: 'Name of the stage'
      }
    ],
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("name of stage"){',
      '      //More config',
      '    }',
      '  }',
      '}'
    ],
    link: ['Stage Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#stage']
  },
  stages: {
    description: 'A group of stages',
    details: 'There must be at least one in a job',
    kind: CompletionItemKind.Method,
    signature: 'stages',
    insertion: 'stages{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Stages Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#stages']
  },
  agent: {
    description: 'A definition for where a stage/job should run',
    details: 'Can be defined once at the pipeline level or in stages themselves',
    kind: CompletionItemKind.Method,
    signature: 'agent',
    insertion: 'agent{\n\n}',
    usage: [
      'pipeline{',
      '  agent{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Agent Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#agent']
  },
  parameters: {
    description: 'A definition for parameters in a parameterized job',
    kind: CompletionItemKind.Method,
    signature: 'parameters',
    insertion: 'parameters{\n\n}',
    usage: [
      'pipeline{',
      '  parameters{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Parameters Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#parameters']
  },
  post: {
    description: 'A definition for actions to perform after stage/job',
    details: 'Can be defined once at the pipeline level or in stages themselves',
    kind: CompletionItemKind.Method,
    signature: 'post',
    insertion: 'post{\n\n}',
    usage: [
      'pipeline{',
      '  post{',
      '    always{ }',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Post Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#post']
  },
  steps: {
    description: 'A definition for actions to perform in a stage',
    kind: CompletionItemKind.Method,
    signature: 'steps',
    insertion: 'steps{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("Example"){',
      '      steps{',
      '        //More config',
      '       }',
      '     }',
      '  }',
      '}'
    ],
    link: ['Steps Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#steps']
  },
  environment: {
    description: 'A definition for environment variables in a stage/job',
    details: 'Can be defined once at the pipeline level or in stages themselves.\nYou cannot override pipeline vars values',
    kind: CompletionItemKind.Method,
    signature: 'environment',
    insertion: 'environment{\n\n}',
    usage: [
      'pipeline{',
      '  environment{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Environment Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#environment']
  },
  options: {
    description: 'A definition for options in a pipeline',
    kind: CompletionItemKind.Method,
    signature: 'options',
    insertion: 'options{\n\n}',
    usage: [
      'pipeline{',
      '  options{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Options Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#options']
  },
  triggers: {
    description: 'A definition for triggers of a pipeline',
    kind: CompletionItemKind.Method,
    signature: 'triggers',
    insertion: 'triggers{\n\n}',
    usage: [
      'pipeline{',
      '  triggers{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Triggers Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#triggers']
  },
  tools: {
    description: 'A definition for tools in a stage/job',
    details: 'Can be defined once at the pipeline level or in stages themselves',
    kind: CompletionItemKind.Method,
    signature: 'tools',
    insertion: 'tools{\n\n}',
    usage: [
      'pipeline{',
      '  tools{',
      '    //More config',
      '  }',
      '}'
    ],
    link: ['Tools Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#tools']
  },
  input: {
    description: 'A definition for user input in a stage',
    details: 'This pauses a build stage to get confirmation or info from a human to continue',
    kind: CompletionItemKind.Method,
    signature: 'input',
    insertion: 'input{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("Example"){',
      '      input{',
      '        //More config',
      '      }',
      '    }',
      '  }',
      '}'
    ],
    link: ['Input Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#input']
  },
  when: {
    description: 'A definition for when a stage should run',
    kind: CompletionItemKind.Method,
    signature: 'when',
    insertion: 'when{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("Example"){',
      '      when{',
      '        //More config',
      '      }',
      '    }',
      '  }',
      '}'
    ],
    link: ['When Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#when']
  },
  parallel: {
    description: 'A definition for a parallel set of stages that should run',
    kind: CompletionItemKind.Method,
    signature: 'parallel',
    insertion: 'parallel{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("Example"){',
      '      parallel{',
      '        //More config',
      '      }',
      '    }',
      '  }',
      '}'
    ],
    link: ['Parallel Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#parallel']
  },
  matrix: {
    description: 'A definition for a matrixed set of stages that should run',
    kind: CompletionItemKind.Method,
    signature: 'matrix',
    insertion: 'matrix{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("Example"){',
      '      matrix{',
      '        //More config',
      '      }',
      '    }',
      '  }',
      '}'
    ],
    link: ['Matrix Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#matrix']
  },
  script: {
    description: 'A definition for a script step',
    kind: CompletionItemKind.Method,
    signature: 'script',
    insertion: 'script{\n\n}',
    usage: [
      'pipeline{',
      '  stages{',
      '    stage("Example"){',
      '      steps{',
      '        script{',
      '          //More config',
      '        }',
      '      }',
      '    }',
      '  }',
      '}'
    ],
    link: ['Script Syntax', 'https://www.jenkins.io/doc/book/pipeline/syntax/#script']
  }
}
