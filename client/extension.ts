'use strict'

import * as path from 'path'
import { ExtensionContext, StatusBarAlignment, StatusBarItem, window, workspace } from 'vscode'
import {
  LanguageClient,
  LanguageClientOptions,
  ServerOptions,
  TransportKind
} from 'vscode-languageclient/node'

let client: LanguageClient
let statusBarItem: StatusBarItem
const statusRegex = /( *)(stage ?\(.+){/gm
let enableStatusBar = false

/*
 *
 * Extension Overrides
 *
 */

export function activate (context: ExtensionContext): void {
  registerStatusBar(context)

  const serverModule = context.asAbsolutePath(
    path.join('dist', 'server', 'server.js')
  )
  client = new LanguageClient(
    'jenkinsfile-lsp',
    'Jenkinsfile Language Server',
    getServerOptions(serverModule),
    getClientOptions()
  )
  client.start().then(() => {}, () => {})
}

export function deactivate (): Thenable<void> | undefined {
  if (client === undefined) {
    return undefined
  }
  return client.stop()
}

/*
 *
 * Private Methods
 *
 */

function getServerOptions (serverModule: string): ServerOptions {
  return {
    run: {
      module: serverModule,
      transport: TransportKind.ipc
    },
    debug: {
      module: serverModule,
      transport: TransportKind.ipc,
      options: {
        execArgv: ['--nolazy', '--inspect=6009']
      }
    }
  }
}

function getClientOptions (): LanguageClientOptions {
  return {
    documentSelector: [
      { language: 'jenkinsfile' },
      { language: 'plaintext' }
    ]
  }
}

function registerStatusBar (context: ExtensionContext): void {
  statusBarItem = window.createStatusBarItem(StatusBarAlignment.Right, Number.MAX_VALUE)
  context.subscriptions.push(statusBarItem)
  context.subscriptions.push(window.onDidChangeTextEditorSelection(updateStatusBar))
  context.subscriptions.push(window.onDidChangeVisibleTextEditors(updateStatusBar))
  context.subscriptions.push(workspace.onDidChangeConfiguration(updateSettingsLocal))
  updateSettingsLocal()
  updateStatusBar()
}

function updateSettingsLocal (): void {
  const settings = workspace.getConfiguration('jenkinsfile')
  enableStatusBar = settings.get('statusBarTracking') ?? false
}

function updateStatusBar (): void {
  if (!enableStatusBar) {
    return
  }
  const te = window.activeTextEditor
  if (te?.document !== undefined && te.document.languageId === 'jenkinsfile') {
    for (let i = te.selection.active.line; i > 0; --i) {
      const line = te.document.lineAt(i)
      const m = statusRegex.exec(line.text)
      if (m !== null) {
        statusBarItem.text = `Section: ${m[2]}`
        statusBarItem.show()
        return
      }
    }
  }
  statusBarItem.text = ''
  statusBarItem.show()
}
