# Jenkinsfile VSCode

> Forked from sgwozdz/jenkinsfile-support (Github)
>
> Gitlab is the main repository. It is pushed (mirrored) to Github

## Features

Extension gives user jenkinsfile support with support for embedded full groovy and yaml.

- syntax highlighting
- status bar updates
- snippets
- settings for control
- document ui enhancements
  - code lens
- hover provider (in progress)
- code completion (in progress)
- signature provider (in progress)
- formatting provider (base implementation)
- validations (base implementation)

**`in progress` items need `LangSpec` built out**

## Coming Features (todo)

- document ui enhancements
  - region painting (decorators)
- completion based on section


## Stretch goals

- semantic language support
- embedded language handoff
- treeview
- jenkins builtin method support
  - recognize
  - hover provider
- shared library support
  - lookups
  - better general support