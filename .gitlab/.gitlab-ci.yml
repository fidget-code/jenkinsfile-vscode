# yaml-language-server: $schema=https://gitlab.com/gitlab-org/gitlab/-/raw/master/app/assets/javascripts/editor/schema/ci.json

default:
  interruptible: false
  image: node:18
  before_script:
    - cd .gitlab
    - wget -q https://github.com/PowerShell/PowerShell/releases/download/v7.4.0/powershell_7.4.0-1.deb_amd64.deb
    - dpkg -i powershell_7.4.0-1.deb_amd64.deb
    - cd ..
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
    - git config --list --show-origin

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"

stages:
  - install
  - build
  - test
  - deploy

install:
  stage: install
  artifacts:
    untracked: true
    expire_in: 20 min
    paths:
      - node_modules/
    reports:
      dotenv: build.env
  script:
    - echo 'Running install'
    - npm ci --include=prod --include=dev

version:
  stage: build
  variables:
    VERSION_TYPE: "patch"
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_COMMIT_REF_NAME != $CI_DEFAULT_BRANCH
      variables:
        VERSION_TYPE: "minor"
    - if: $CI_PIPELINE_SOURCE == "push"
  needs:
    - job: install
      artifacts: true
  artifacts:
    untracked: true
    expire_in: 20 min
    reports:
      dotenv: build.env
    paths:
      - project.json
  script:
    - npm --no-git-tag-version version $VERSION_TYPE
    - export VERSION=$(node -p "require('./package.json').version")
    - echo "VERSION=${VERSION}" >> build.env
    - git add package.json
    - "git commit -m 'ci version update'"
    - git remote set-url --push origin "https://$CI_JOB_TOKEN_CUSTOM@gitlab.com/$CI_PROJECT_PATH.git"
    - git push origin HEAD:$CI_COMMIT_REF_NAME

build:
  stage: build
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
  needs:
    - job: install
      artifacts: true
    - job: version
      artifacts: true
  artifacts:
    untracked: true
    expire_in: 20 min
    paths:
      - dist/
  script:
    - echo 'Running build'
    - npm run esbuild:prod

test:
  stage: test
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
  needs:
    - job: install
      artifacts: true
    - job: build
      artifacts: true
    - job: version
      artifacts: true
  artifacts:
    when: always
    reports:
      junit:
        - coverage/junit.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura-coverage.xml
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  script:
    - echo 'Running test'
    - npm run test
    - npm run coverage

package-dev:
  stage: deploy
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  needs:
    - job: test
      artifacts: false
    - job: install
      artifacts: true
    - job: build
      artifacts: true
    - job: version
      artifacts: true
  artifacts:
    untracked: false
    name: "jenkins-vscode-${VERSION}"
    paths:
      - "**/*.vsix"
    when: on_success
    expire_in: 20 min
  script:
    - echo 'Running package'
    - npm ci --include=prod --include=dev
    - npm run pack:ci
    - npx vsce package $VERSION

package-prod:
  stage: deploy
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
  needs:
    - job: test
      artifacts: false
    - job: install
      artifacts: true
    - job: build
      artifacts: true
    - job: version
      artifacts: true
  artifacts:
    untracked: false
    name: "jenkins-vscode-${VERSION}"
    paths:
      - "**/*.vsix"
    when: on_success
    expire_in: 20 min
  script:
    - echo 'Running package'
    - npm run licenses
    - npm run changelog
    - npm run pack

upload-gitlab:
  stage: deploy
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
  image: curlimages/curl:latest
  needs:
    - job: package-dev
      artifacts: true
      optional: true
    - job: package-prod
      artifacts: true
      optional: true
    - job: version
      artifacts: true
  before_script:
    - echo "Clean before script"
  script:
    - ls -R
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jenkinsfile-vscode-${VERSION}.vsix "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/jenkinsfile-vscode/${VERSION}/jenkinsfile-vscode-${VERSION}.vsix"'

publish-gitlab:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
  needs:
    - job: version
      artifacts: true
    - job: upload-gitlab
      artifacts: false
  before_script:
    - echo "Clean before script"
  script:
    - echo "Running the release job."
  release:
    tag_name: $VERSION
    description: Release to GitLab
    name: Release $VERSION
    assets:
      links:
        - name: jenkinsfile-vscode-${VERSION}.visx
          url: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/jenkinsfile-vscode/${VERSION}/jenkinsfile-vscode-${VERSION}.vsix

publish:
  stage: deploy
  needs:
    - job: install
      artifacts: true
    - job: version
      artifacts: true
    - job: package-prod
      artifacts: true
    - job: publish-gitlab
      artifacts: false
  rules:
    - if: $CI_COMMIT_MESSAGE =~ /ci version update/
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: never
    - if: $CI_PIPELINE_SOURCE == "push"
  script:
    - echo 'Running publish'
    - npm run deploy
    - git tag $VERSION
    - git push origin --tags
