#### [2.0.23](https://gitlab.com/fidget-code/jenkinsfile-vscode/compare/2.0.22...2.0.23)



**Changes**

- Merge branches 'master' and 'master' of gitlab.com:fidget-code/jenkinsfile-vscode [`daa8cf3`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/daa8cf3a0c3990ed7aa894caae6dc9b19cca35a4)
- updates [`29c47a2`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/29c47a2be6ecda2a0fc3b6b439114208d0d60bbd)

#### 2.0.22

> 14 February 2024


**Changes**

- updates [`689b763`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/689b763d6f0fc4582f87d5990ac9f72d29005ab9)
- updates [`d1e3d7e`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/d1e3d7e1220af6c7bb3e3969c09b572eb9f5a4e8)
- updates [`41ede01`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/41ede01c238a0a1bf755daa2044308b1dd72feec)
- updates [`7cf64fa`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/7cf64fa004596600f40e6cd1f62e99727a515822)
- updates [`791bf5f`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/791bf5f0c1d20c745913381abb541b77b5dbda50)
- updates [`43c0b1d`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/43c0b1d4b6043404de05e130c264d2cbffbba361)
- updates [`83c27e6`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/83c27e6569174d9a973a2bfd1962fa28264833a3)
- updates [`7716635`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/7716635fdf8a4d75e422c0780dbeee9219d05bd2)
- updates [`089d117`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/089d117a6576afbe1622358647daa6e240a1ece1)
- updates [`adbd85d`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/adbd85d6c4e7c593c0e4ddca8232244dee680c82)
- updates [`40f0a4d`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/40f0a4d022896e885c873a1851bbb6476ff2e73d)
- updates [`a0ec3c9`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/a0ec3c90d925ea4725e41144a34d3d5f018fc175)
- updates [`b71b830`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/b71b8303f7a5c5913d1becad7cb2cf2adb3ed87c)
- updates [`022ae63`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/022ae634ff2f380fc74ca1f397c539ba996dd462)
- updates [`441fee0`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/441fee0643cc7f1e0d69531892def0fe88d524df)
- updates [`7bff4ee`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/7bff4ee3b5f4ead4c9a5e7e819cc00760298a4ce)
- updates [`ebe16cb`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/ebe16cbf93b4856f7eba44a9323d809659250ad6)
- updates [`ffcf7a8`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/ffcf7a89d5a2f7ece7d992475777d5ebc01849d8)
- updates [`fd29717`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/fd2971728d7ea58f1793fdd3d4b599a18bc0f2ec)
- updates [`22db1f2`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/22db1f22dd1fc26487a9b0403ef0c310264e00f3)
- updates [`231683c`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/231683c92540cdcf780e3bfde4bd795edafedc2f)
- updates [`6ff30e0`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/6ff30e07625bd097e9ba073ee833b55245ce52e0)
- updates [`e29c4f4`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/e29c4f4efe9a33bd881262fbf76cdbfe80c5f6ac)
- updates [`dbfec2b`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/dbfec2b725a7c799aeee7b420cbaf6af22355b0f)
- updates [`4f240a9`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/4f240a9e23ff55c3f7f13a370f5a4b3e8cab0af6)
- add test basics [`4dde564`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/4dde564d80f0138fd6d73894a7cdd816b4f69661)
- packaging update [`3de6b60`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/3de6b606d11b8e9900f91aa9c394d674fcab65f0)
- restructure [`7194e2b`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/7194e2bc07ca8fab9ca0dd179a706d699eb743e7)
- updates [`be499e5`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/be499e52ba719ca0e0a5d8242b71ece0b2fcf8b6)
- base language syntax highlighting [`1af61e2`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/1af61e25d820ace04f2e85ea1c0c4020a28044f8)
- progress [`069c3c5`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/069c3c5b26d18d0489ff228730b7207e17501560)
- update debug [`4ed3e6b`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/4ed3e6b2ce6d7d630aa8904ec8e256308271f81e)
- updates [`a379a14`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/a379a141f185db0a91c132e19ccc5300ebda959f)
- updates [`3f91d2e`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/3f91d2e915af546a66dc548f426857ca6edcb968)
- base fixes and restructure [`2a814b2`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/2a814b26091322f584646066aee2370cee1e3c36)
- updates [`a819acc`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/a819acc14e859ad500ebb579c82d271379a28666)
- updates to fork [`1aabd86`](https://gitlab.com/fidget-code/jenkinsfile-vscode/commit/1aabd86df0e01f37cab276a85bae36b93c5acd1a)

